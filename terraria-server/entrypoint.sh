#!/usr/bin/env bash

THIS_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"

main() {
  set -x

  if [[ "$1" == "*TerrariaServer*" ]]; then
    exec "$@" -worldname "${WORLDNAME-${HOSTNAME}}"
  else
    exec "$@"
  fi
}

main "$@"
